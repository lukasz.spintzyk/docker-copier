# docker-copier

Using https://copier.readthedocs.io to autogenerate docker based shell environment


## ToDo
* Add script to upload docker image to docker hub
* When docker image does not exists try to download it from docker hub first
* add gitlab-ci/jenkinsfile to rebuild docker image on tag update or git tag

## Done
* Add docker image build [Done]
* Change activate script to run only [Done]
* Add containerId to local .gitignore and add attach script [Done]
* Run as nonroot [Done]


